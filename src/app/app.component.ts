import {Component, OnInit} from '@angular/core';
import {UserProfile} from './user-profile/user-profile';
import {UserProfilesService} from './user-profile/user-profiles.service';
import {AppMessage} from './common/app-message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  title = 'Bill Jobs Catering';
  selectedUserProfile: UserProfile;
  localAlert: AppMessage;
  private localAlertTimer: any;
  constructor(private userProfilesService: UserProfilesService) {
    this.selectedUserProfile = null;
    this.localAlert = null;
  }

  setMessage(appMessage: AppMessage): void {
    this.localAlert = appMessage;
    if (!!this.localAlertTimer){
      clearTimeout(this.localAlertTimer);
    }
    this.localAlertTimer = setTimeout(() => {
      this.localAlert = null;
    }, 5000);
  }

  ngOnInit() {
    this.userProfilesService.getMessages()
      .subscribe(x => {
        this.setMessage(x);
      });
  }

  dismissAlert(): void {
    this.localAlert = null;
  }
}
