import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserProfile} from '../user-profile/user-profile';
import {UserProfilesService} from '../user-profile/user-profiles.service';
import {UserProfilesState} from '../user-profile/user-profiles-state';
import {Store} from '@ngrx/store';
import {UserProfileDeleteAction} from '../user-profile/user-profiles-actions';
import {getUserProfiles} from '../user-profile/user-profiles-reducer';

@Component({
  selector: 'app-user-profile-details',
  templateUrl: './user-profile-details.component.html',
  styleUrls: ['./user-profile-details.component.scss']
})
export class UserProfileDetailsComponent implements OnInit, OnDestroy {
  userProfile: UserProfile;
  private selectedUerSubscription: any;
  constructor(private userProfilesService: UserProfilesService,
              private userProfilesStore: Store<UserProfilesState>) {
    this.userProfile = null;
  }

  ngOnInit() {
    this.selectedUerSubscription = this.userProfilesService.getSelectedUserProfile()
      .subscribe(
        x => {
          this.userProfile = x;
        },
        x => {
          alert(x);
        });
  }

  ngOnDestroy() {
    this.selectedUerSubscription.unsubscribe();
  }

  btnTrashClick(): void {
    this.userProfilesStore.select(getUserProfiles)
      .subscribe(
        x => {
          // nothing to do
        },
        x => {
          alert(x);
        });

    this.userProfilesStore.dispatch(new UserProfileDeleteAction(this.userProfile.userId));
  }

  btnEditClick(): void {
    this.userProfilesService.setEditUserProfileEvent(this.userProfile);
  }
}
