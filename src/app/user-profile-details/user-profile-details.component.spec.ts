import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileDetailsComponent } from './user-profile-details.component';
import {UserProfileEditComponent} from '../user-profile-edit/user-profile-edit.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserProfilesService} from '../user-profile/user-profiles.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from '../common/app-state';

describe('UserProfileDetailsComponent', () => {
  let component: UserProfileDetailsComponent;
  let fixture: ComponentFixture<UserProfileDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserProfileDetailsComponent,
        UserProfileEditComponent
      ],
      providers: [
        UserProfilesService
      ],
      imports: [
        MDBBootstrapModule.forRoot(),
        FormsModule,
        HttpClientTestingModule,
        StoreModule.forRoot(reducers, {metaReducers}),
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
