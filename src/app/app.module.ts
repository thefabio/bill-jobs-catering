import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppComponent } from './app.component';
import {UserProfilesService} from './user-profile/user-profiles.service';
import { UserProfileListComponent } from './user-profile-list/user-profile-list.component';
import { UserProfileDetailsComponent } from './user-profile-details/user-profile-details.component';
import {HttpClientModule} from '@angular/common/http';
import { UserProfileNewComponent } from './user-profile-new/user-profile-new.component';
import {FormsModule} from '@angular/forms';
import { UserProfileEditComponent } from './user-profile-edit/user-profile-edit.component';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from './common/app-state';
import {EffectsModule} from '@ngrx/effects';
import {UserProfileEffects} from './user-profile/user-profile-effects';


@NgModule({
  declarations: [
    AppComponent,
    UserProfileListComponent,
    UserProfileDetailsComponent,
    UserProfileNewComponent,
    UserProfileEditComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot([ UserProfileEffects])
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    UserProfilesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
