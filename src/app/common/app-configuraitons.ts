export class AppConfigurations {
  public static scopeKey = 'orieos_at_gmail.com';

  public static apiEndPoints(): any {
    const rootApi = 'https://zware-ngnewapi.azurewebsites.net';

    const endPoints = {
      userProfiles: 'profiles'
    };

    for (const key in endPoints) {
      endPoints[key] = [rootApi, 'api', AppConfigurations.scopeKey, endPoints[key]].join('/');
    }

    return endPoints;
  }
}
