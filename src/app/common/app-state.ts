import {ActionReducerMap, MetaReducer} from '@ngrx/store';
import {UserProfilesState} from '../user-profile/user-profiles-state';
import {UserProfilesReducer} from '../user-profile/user-profiles-reducer';

export interface AppState {
  userProfilesState: UserProfilesState;
}
export const reducers: ActionReducerMap<AppState> = {
  userProfilesState: UserProfilesReducer.reducer
};

export const metaReducers: MetaReducer<AppState>[] = [];
