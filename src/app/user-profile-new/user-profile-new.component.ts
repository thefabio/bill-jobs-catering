import {Component, OnInit, ViewChild} from '@angular/core';
import {UserProfile} from '../user-profile/user-profile';
import {UserProfilesState} from '../user-profile/user-profiles-state';
import {Store} from '@ngrx/store';
import {UserProfileCreateAction} from '../user-profile/user-profiles-actions';
import {getUserProfiles} from '../user-profile/user-profiles-reducer';

@Component({
  selector: 'app-user-profile-new',
  templateUrl: './user-profile-new.component.html',
  styleUrls: ['./user-profile-new.component.scss']
})
export class UserProfileNewComponent implements OnInit {
  newUserProfile: UserProfile;
  @ViewChild('newUserProfileModal') newUserProfileModal;

  constructor(private userProfilesStore: Store<UserProfilesState>) {
    this.newUserProfile = {} as UserProfile;
  }

  ngOnInit() {
  }

  btnSaveClick(): void{
    this.userProfilesStore.select(getUserProfiles)
      .subscribe(
        x => {
          this.newUserProfileModal.hide();
        },
        x => {
          alert(x);
        }
      );

    this.userProfilesStore.dispatch(new UserProfileCreateAction(this.newUserProfile));
  }
}
