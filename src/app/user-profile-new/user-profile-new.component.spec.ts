import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileNewComponent } from './user-profile-new.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserProfilesService} from '../user-profile/user-profiles.service';
import {FormsModule} from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from '../common/app-state';

describe('UserProfileNewComponent', () => {
  let component: UserProfileNewComponent;
  let fixture: ComponentFixture<UserProfileNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileNewComponent ],
      providers: [
        UserProfilesService
      ],
      imports: [
        MDBBootstrapModule.forRoot(),
        FormsModule,
        HttpClientTestingModule,
        StoreModule.forRoot(reducers, {metaReducers}),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
