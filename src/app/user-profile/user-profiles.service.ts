import { Injectable } from '@angular/core';
import {AppConfigurations} from '../common/app-configuraitons';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {UserProfile} from './user-profile';
import {Subject} from 'rxjs/Subject';
import {AppMessage} from '../common/app-message';

@Injectable()
export class UserProfilesService {
  private _serviceUrl: string;
  private getAllSubject = new Subject<UserProfile[]>();
  private selectUserSubject = new Subject<UserProfile>();
  private editUserProfile = new Subject<UserProfile>();
  private messageSubject = new Subject<AppMessage>();

  get serviceUrl(): string {
    return this._serviceUrl;
  }

  constructor(private http: HttpClient) {
    this._serviceUrl = AppConfigurations.apiEndPoints().userProfiles;
  }

  getAll(): Observable<UserProfile[]> {
    this.triggerGetAll();
    return this.getAllSubject.asObservable();
  }

  private triggerGetAll(): void {
    this.http.get<UserProfile[]>(this._serviceUrl)
      .subscribe(
        x => {
        this.getAllSubject.next(x);
        },
        x => {
          this.getAllSubject.error(x);
        });
  }

  setSelectUserProfile(userProfileId: number): void {
    if (userProfileId === null) {
      this.selectUserSubject.next(null);
      return;
    }

    this.http.get<UserProfile>([this._serviceUrl, userProfileId].join('/'))
      .subscribe(
        x => {
          this.selectUserSubject.next(x);
        },
        x => {
          this.selectUserSubject.error(x);
        });
  }

  getSelectedUserProfile(): Observable<UserProfile> {
    return this.selectUserSubject.asObservable();
  }

  create(userProfile: UserProfile): Observable<any> {
    const createSubject = new Subject<any>();
    this.http.post(this._serviceUrl, userProfile)
      .subscribe(
        x => {
          this.triggerGetAll();
          createSubject.next(x);
          this.sendMessage(true, 'User Profile created');
        },
        x => {
          this.sendMessage(false, 'User Profile could not be created');
          createSubject.error(x);
        }
      );
    return createSubject.asObservable();
  }

  update(userProfile: UserProfile): Observable<any> {
    const updateSubject = new Subject<any>();
    this.http.put([this._serviceUrl, userProfile.userId].join('/'), userProfile)
      .subscribe(
        x => {
          updateSubject.next(x);
          this.triggerGetAll();
          this.setSelectUserProfile(userProfile.userId);
          this.sendMessage(true, 'User Profile Updated');
        },
        x => {
          updateSubject.error(x);
          this.sendMessage(false, 'User Profile could not be Updated');
        }
      );

    return updateSubject.asObservable();
  }

  destroy(userProfileId: number): Observable<any> {
    const destroySubject = new Subject<any>();
    this.http.delete([this._serviceUrl, userProfileId].join('/'))
      .subscribe(
        x => {
          this.triggerGetAll();
          destroySubject.next(x);
          this.selectUserSubject.next(null);
          this.sendMessage(true, 'User Profile Removed');
        },
        x => {
          this.sendMessage(false, 'User Profile could not be removed');
          destroySubject.error(x);
        });
    return destroySubject.asObservable();
  }

  private sendMessage(success: boolean, msg: string) {
    this.messageSubject.next({success: success, msg: msg} as AppMessage);
  }

  getMessages(): Observable<AppMessage> {
    return this.messageSubject.asObservable();
  }

  setEditUserProfileEvent(userProfile: UserProfile): void {
    this.editUserProfile.next(userProfile);
  }

  getEditUserProfileEvent(): Observable<UserProfile> {
    return this.editUserProfile.asObservable();
  }
}
