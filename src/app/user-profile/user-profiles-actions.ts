import {Action} from '@ngrx/store';
import {UserProfile} from './user-profile';

export class UserProfilesGetAction implements Action {
  static readonly ACTION_TYPE = 'user-profiles-get';
  readonly type = UserProfilesGetAction.ACTION_TYPE;
  constructor(public payload: UserProfile[]) {}
}

export class UserProfilesGetSuccessAction implements Action {
  static readonly ACTION_TYPE = 'user-profiles-get-success';
  readonly type = UserProfilesGetSuccessAction.ACTION_TYPE;
  constructor(public payload: UserProfile[]) {}
}
export class UserProfilesGetFailureAction implements Action {
  static readonly ACTION_TYPE = 'user-profiles-get-failure';
  readonly type = UserProfilesGetFailureAction.ACTION_TYPE;
  constructor(public payload: any) {}
}

export class UserProfileCreateAction implements Action {
  static readonly ACTION_TYPE = 'user-profile-create';
  readonly type = UserProfileCreateAction.ACTION_TYPE;
  constructor(public payload: UserProfile) {}
}
export class UserProfileCreateFailureAction implements Action {
  static readonly ACTION_TYPE = 'user-profile-create-failure';
  readonly type = UserProfileCreateFailureAction.ACTION_TYPE;
  constructor(public payload: any) {}
}

export class UserProfileUpdateAction implements Action {
  static readonly ACTION_TYPE = 'user-profile-update';
  readonly type = UserProfileUpdateAction.ACTION_TYPE;
  constructor(public payload: UserProfile) {}
}
export class UserProfileUpdateFailureAction implements Action {
  static readonly ACTION_TYPE = 'user-profile-update-failure';
  readonly type = UserProfileUpdateFailureAction.ACTION_TYPE;
  constructor(public payload: any) {}
}

export class UserProfileDeleteAction implements Action {
  static readonly ACTION_TYPE = 'user-profile-delete';
  readonly type = UserProfileDeleteAction.ACTION_TYPE;
  constructor(public payload: number) {}
}
export class UserProfileDeleteFailureAction implements Action {
  static readonly ACTION_TYPE = 'user-profile-delete-failure';
  readonly type = UserProfileDeleteFailureAction.ACTION_TYPE;
  constructor(public payload: any) {}
}


export type AllUserProfilesActions = UserProfilesGetAction | UserProfilesGetSuccessAction | UserProfilesGetFailureAction;
