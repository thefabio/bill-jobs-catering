import { TestBed, inject } from '@angular/core/testing';
import { UserProfilesService } from './user-profiles.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {AppConfigurations} from '../common/app-configuraitons';
import {UserProfile} from './user-profile';

describe('UserProfilesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [UserProfilesService]
    });
  });

  it('should be created', inject([UserProfilesService], (service: UserProfilesService) => {
    expect(service).toBeTruthy();
  }));

  it('service URL should be mapped correctly', inject([UserProfilesService], (service: UserProfilesService) => {
    expect(service.serviceUrl).toBe(AppConfigurations.apiEndPoints().userProfiles);
  }));

  describe('service calls', () => {
    let userProfilesService = null;
    let httpMock = null;
    beforeEach(() => {
      userProfilesService = TestBed.get(UserProfilesService);
      httpMock = TestBed.get(HttpTestingController);
    });

    it ('retrieves all user profiles with getAll', done => {
      userProfilesService.getAll().subscribe(x => {
        expect(x.length).toBe(3);
        done();
      });

      const mockRequest = httpMock.expectOne(AppConfigurations.apiEndPoints().userProfiles);
      mockRequest.flush([
        {userId: 1, firstName: 'Marco', lastName: 'Pollo'},
        {userId: 2, firstName: 'Brett', lastName: 'Joe'},
        {userId: 3, firstName: 'Alice', lastName: 'Brow'},
      ] as UserProfile[]);
      httpMock.verify();
    });

    it ('retrieve one user profile with the subscription model provided by setSelectUserProfile/getSelectedUserProfile', done => {
      const testUserProfile = {userId: 53, firstName: 'Marco', lastName: 'Pollo'} as UserProfile;
      userProfilesService.getSelectedUserProfile().subscribe(x => {
        expect(x.userId).toBe(testUserProfile.userId);
        expect(x.firstName).toBe(testUserProfile.firstName);
        done();
      });

      userProfilesService.setSelectUserProfile(testUserProfile.userId);

      const mockRequest = httpMock.expectOne([AppConfigurations.apiEndPoints().userProfiles, testUserProfile.userId].join('/'));
      mockRequest.flush(testUserProfile);
      httpMock.verify();
    });

    it ('creates a new user profiles with create', done => {
      const testUserProfile = {firstName: 'Marco', lastName: 'Pollo'} as UserProfile;
      userProfilesService.create(testUserProfile).subscribe(x => {
        expect(x.userId).not.toBeNull();
        expect(x.firstName).toBe(testUserProfile.firstName);
        done();
      });

      const mockRequest1 = httpMock.expectOne(AppConfigurations.apiEndPoints().userProfiles);
      mockRequest1.flush({userId: 53, firstName: 'Marco', lastName: 'Pollo'} as UserProfile);

      const mockRequest2 = httpMock.expectOne(AppConfigurations.apiEndPoints().userProfiles);
      mockRequest2.flush([{userId: 53, firstName: 'Marco', lastName: 'Pollo'} as UserProfile]);

      httpMock.verify();
    });

    it ('updates a user profiles with update', done => {
      const testUserProfile = {userId: 53, firstName: 'Marco', lastName: 'Pollo'} as UserProfile;
      userProfilesService.update(testUserProfile).subscribe(x => {
        expect(x).toBeNull();
        done();
      });

      const mockRequest1 = httpMock.expectOne([AppConfigurations.apiEndPoints().userProfiles, testUserProfile.userId].join('/'));
      mockRequest1.flush(null);

      const mockRequest2 = httpMock.expectOne(AppConfigurations.apiEndPoints().userProfiles);
      mockRequest2.flush([testUserProfile]);

      const mockRequest3 = httpMock.expectOne([AppConfigurations.apiEndPoints().userProfiles, testUserProfile.userId].join('/'));
      mockRequest3.flush(testUserProfile);

      httpMock.verify();
    });

    it ('deletes a user profiles with destroy', done => {
      const testUserProfile = {userId: 53, firstName: 'Marco', lastName: 'Pollo'} as UserProfile;
      userProfilesService.destroy(testUserProfile.userId).subscribe(x => {
        expect(x).toBeNull();
        done();
      });

      const mockRequest = httpMock.expectOne([AppConfigurations.apiEndPoints().userProfiles, testUserProfile.userId].join('/'));
      mockRequest.flush(null);

      const mockRequest2 = httpMock.expectOne(AppConfigurations.apiEndPoints().userProfiles);
      mockRequest2.flush([testUserProfile]);

      httpMock.verify();
    });
  });
});
