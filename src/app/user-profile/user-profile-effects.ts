import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {
  UserProfileCreateAction, UserProfileCreateFailureAction, UserProfileDeleteAction, UserProfileDeleteFailureAction,
  UserProfilesGetAction, UserProfilesGetFailureAction,
  UserProfilesGetSuccessAction, UserProfileUpdateAction, UserProfileUpdateFailureAction
} from './user-profiles-actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {UserProfilesService} from './user-profiles.service';
import {of} from 'rxjs/observable/of';

@Injectable()
export class UserProfileEffects {
  constructor(private actions: Actions,
  private userProfilesService: UserProfilesService ) {}

  @Effect()
  getAllUserProfiles$: Observable<Action> = this.actions
    .ofType<UserProfilesGetAction>(UserProfilesGetAction.ACTION_TYPE)
    .switchMap((a) => this.userProfilesService.getAll()
        .map(x => new UserProfilesGetSuccessAction(x))
        .catch(x => of(new UserProfilesGetFailureAction(x)))
  );

  @Effect()
  createUserProfile$: Observable<Action> = this.actions
    .ofType<UserProfileCreateAction>(UserProfileCreateAction.ACTION_TYPE)
    .map(action => action.payload)
    .mergeMap((a) => this.userProfilesService.create(a)
      .map(x => new UserProfilesGetAction(null))
      .catch(x => of(new UserProfileCreateFailureAction(x)))
    );

  @Effect()
  updateUserProfile$: Observable<Action> = this.actions
    .ofType<UserProfileUpdateAction>(UserProfileUpdateAction.ACTION_TYPE)
    .map(action => action.payload)
    .switchMap((a) => this.userProfilesService.update(a)
      .map(x => new UserProfilesGetAction(null))
      .catch(x => of(new UserProfileUpdateFailureAction(x)))
    );

  @Effect()
  deleteUserProfile$: Observable<Action> = this.actions
    .ofType<UserProfileDeleteAction>(UserProfileDeleteAction.ACTION_TYPE)
    .map(action => action.payload)
    .switchMap((a) => this.userProfilesService.destroy(a)
      .map(x => new UserProfilesGetAction(null))
      .catch(x => of(new UserProfileDeleteFailureAction(x)))
    );
}
