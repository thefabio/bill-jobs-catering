import {UserProfile} from './user-profile';

export interface UserProfilesState {
  userProfiles: UserProfile[];
}
