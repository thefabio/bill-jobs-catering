import {UserProfilesState} from './user-profiles-state';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {
  AllUserProfilesActions, UserProfilesGetAction, UserProfilesGetFailureAction,
  UserProfilesGetSuccessAction
} from './user-profiles-actions';

export class UserProfilesReducer {
  private static initialState: UserProfilesState = { userProfiles: []};

  static reducer(state = UserProfilesReducer.initialState, action: AllUserProfilesActions): UserProfilesState {
    switch (action.type) {
      case UserProfilesGetAction.ACTION_TYPE: {
        return {
          userProfiles: action.payload
        };
      }

      case UserProfilesGetSuccessAction.ACTION_TYPE: {
        return {
          userProfiles: action.payload
        };
      }

      case UserProfilesGetFailureAction.ACTION_TYPE: {
        return {
          userProfiles: action.payload
        };
      }

      default: {
        return state;
      }
    }
  }
}

export const getUserProfilesState = createFeatureSelector<UserProfilesState>('userProfilesState');

export const getUserProfiles = createSelector(
    getUserProfilesState,
    (x: UserProfilesState) => x.userProfiles
  );
