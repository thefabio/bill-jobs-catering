import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserProfile} from '../user-profile/user-profile';
import {UserProfilesState} from '../user-profile/user-profiles-state';
import {Store} from '@ngrx/store';
import {UserProfileUpdateAction} from '../user-profile/user-profiles-actions';
import {getUserProfiles} from '../user-profile/user-profiles-reducer';
import {UserProfilesService} from '../user-profile/user-profiles.service';

@Component({
  selector: 'app-user-profile-edit',
  templateUrl: './user-profile-edit.component.html',
  styleUrls: ['./user-profile-edit.component.scss']
})
export class UserProfileEditComponent implements OnInit, OnDestroy {
  @ViewChild('editUserProfileModal') editUserProfileModal;
  private editUserSubscription: any;
  userProfile: UserProfile;

  constructor(private userProfilesService: UserProfilesService,
    private userProfilesStore: Store<UserProfilesState>) {
   }

  ngOnInit() {
    this.editUserSubscription = this.userProfilesService.getEditUserProfileEvent()
      .subscribe(x => {
        this.userProfile = x;
        this.editUserProfileModal.show();
      });
  }

  ngOnDestroy() {
    this.editUserSubscription.unsubscribe();
  }

  btnSaveClick() {
    this.userProfilesStore.select(getUserProfiles)
      .subscribe(
        x => {
          this.editUserProfileModal.hide();
        },
        x => {
          alert(x);
        });

    this.userProfilesStore.dispatch(new UserProfileUpdateAction(this.userProfile));
  }
}
