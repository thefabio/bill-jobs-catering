import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileEditComponent } from './user-profile-edit.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule} from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {UserProfilesService} from '../user-profile/user-profiles.service';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from '../common/app-state';

describe('UserProfileEditComponent', () => {
  let component: UserProfileEditComponent;
  let fixture: ComponentFixture<UserProfileEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileEditComponent ],
      providers: [
        UserProfilesService
      ],
      imports: [
        MDBBootstrapModule.forRoot(),
        FormsModule,
        HttpClientTestingModule,
        StoreModule.forRoot(reducers, {metaReducers}),
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
