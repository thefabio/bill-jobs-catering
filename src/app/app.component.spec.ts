import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {UserProfileDetailsComponent} from './user-profile-details/user-profile-details.component';
import {UserProfileListComponent} from './user-profile-list/user-profile-list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {UserProfilesService} from './user-profile/user-profiles.service';
import {UserProfileEditComponent} from './user-profile-edit/user-profile-edit.component';
import {UserProfileNewComponent} from './user-profile-new/user-profile-new.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {metaReducers, reducers} from './common/app-state';
import {StoreModule} from '@ngrx/store';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        UserProfileListComponent,
        UserProfileDetailsComponent,
        UserProfileNewComponent,
        UserProfileEditComponent,
      ],
      providers: [
        UserProfilesService
      ],
      imports: [
        MDBBootstrapModule.forRoot(),
        HttpClientTestingModule,
        StoreModule.forRoot(reducers, {metaReducers}),
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Bill Jobs Catering'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Bill Jobs Catering');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Bill Jobs Catering!');
  }));
});
