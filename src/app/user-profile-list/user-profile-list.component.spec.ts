import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileListComponent } from './user-profile-list.component';
import {UserProfilesService} from '../user-profile/user-profiles.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from '../common/app-state';

describe('UserProfileListComponent', () => {
  let component: UserProfileListComponent;
  let fixture: ComponentFixture<UserProfileListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileListComponent ],
      providers: [
        UserProfilesService
      ],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(reducers, {metaReducers}),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
