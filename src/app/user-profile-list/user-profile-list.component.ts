import {Component, Input, OnInit} from '@angular/core';
import {UserProfile} from '../user-profile/user-profile';
import {UserProfilesService} from '../user-profile/user-profiles.service';
import {UserProfilesState} from '../user-profile/user-profiles-state';
import {Store} from '@ngrx/store';
import {getUserProfiles} from '../user-profile/user-profiles-reducer';
import {UserProfilesGetAction} from '../user-profile/user-profiles-actions';

@Component({
  selector: 'app-user-profile-list',
  templateUrl: './user-profile-list.component.html',
  styleUrls: ['./user-profile-list.component.scss']
})
export class UserProfileListComponent implements OnInit {
  @Input()
  userProfiles: UserProfile[];
  selectedUserId: number;

  constructor(private userProfilesService: UserProfilesService,
              private userProfilesStore: Store<UserProfilesState>) {
    this.userProfiles = null;
  }

  reloadStore(): void {
    this.userProfilesStore.dispatch(new UserProfilesGetAction(null));

    setTimeout(() => this.reloadStore(), 30000);
  }

  ngOnInit() {
    this.userProfilesStore.select(getUserProfiles)
      .subscribe(
        x => {
          this.userProfiles = x;
        },
        x => {
          alert(x);
        });

      this.reloadStore();
    }

  btnViewClick(userProfileId): void {
    this.selectedUserId = userProfileId;
    this.userProfilesService.setSelectUserProfile(userProfileId);
  }
}
